﻿using UnityEngine;
using System.Collections;

public class GAv3Control : MonoBehaviour {

    private static GAv3Control _instance;

    public static GAv3Control instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = GameObject.FindObjectOfType<GAv3Control>();
            }
            return _instance;
        }
    }
    void Awake()
    {
        if (_instance == null)
        {
            _instance = this;
        }
        else
        {
            if (this != _instance)
                Destroy(this.gameObject);
        }
      
    }
}
