﻿using UnityEngine;
using System.Collections;
using UnityEditor;

public class ClearP : MonoBehaviour {

	[MenuItem ("PIXA/Clear PlayerPrefs")]
	static void Clear()
	{
		PlayerPrefs.DeleteAll ();
	}
}
