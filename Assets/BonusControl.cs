﻿using UnityEngine;
using System.Collections;

public class BonusControl : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void Click()
    {
        GameManager.instance.hints += 5;
        GameManager.instance.ShowBonus(false);
        GameManager.instance.ShowMenu();
    }
}
