﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class ItemCategoryControl : ItemControl {

    public List<Sprite> sprites;
	public string alias;
    public int size;
    public int brain;
    public bool unlock = false;
	public int category;


	public void Click()
	{
        if (category <= GameManager.instance._max_category ) {
			//print (category);
            if(GameManager.instance.data.subjects[category].show)
            {
                GameManager.instance._category = category;
                GameManager.instance._size = size;
                GameManager.instance.Next();
                GameManager.instance.ShowLevel(true);
            }
            else
            {
                print("Unlock Later!");
                GameManager.instance.ShowUnlock(true);
            }
			
		}


	}

	public void Init(string name,int size,int brain,int category)
	{
		this.alias = name;
		this.size = size;
		this.brain = brain;
		this.category = category;
		Show ();
	}

	public override void Show()
    {
        var res = transform.Find("res");
		var txt_name = transform.Find ("txt_name");
		if (txt_name != null) {
			txt_name.GetComponent<Text> ().text = alias.ToUpper ();
		}
        var txt_name_number = transform.Find("txt_size_number");
        var txt_brain_number = transform.Find("txt_brain_number");
        var arrow = transform.Find("arrow");
        if (res != null)
        {
            var r = Random.Range(0, sprites.Count - 1);
			if (category <= sprites.Count)
				res.GetComponent<Image> ().sprite = sprites [category - 1];
        }
        if (txt_name_number!=null)
        {
            txt_name_number.GetComponent<Text>().text = size + "x" + size;
        }
        if(txt_brain_number!=null)
        {
			var cn = GameManager.instance.data.subjects [category - 1].words.Count * brain;
			txt_brain_number.GetComponent<Text>().text = cn.ToString();
        }

        if(arrow!=null)
        {
            //print("arrow");
            arrow.GetComponent<Button>().interactable = unlock;
        }
    }
}
