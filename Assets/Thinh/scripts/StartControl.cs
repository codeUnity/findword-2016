﻿using UnityEngine;
using System.Collections;

public class StartControl : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void Slide()
	{
		GetComponent<Animator> ().enabled = true;
	}

	public void EndSlide()
	{
		Destroy (gameObject, 0.5f);
	}
}
