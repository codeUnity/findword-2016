﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;


[Serializable]
public class Subject {
	public string name;
	public int size;
	public int brain;
	public List<string> words;
	public int hint;
	public bool solve;
    public bool show;
}

[Serializable]
public class Data {
	public int version;
	[SerializeField]
	public List<Subject> subjects;

}



