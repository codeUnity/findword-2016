﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using DG.Tweening;

public class AnswerControl : MonoBehaviour {

    public int size;
    public string word = "IDEA";
    public GameObject prefabs;
	public GameObject star;
    List<GameObject> items = new List<GameObject>();
	float cell;

    Dictionary<int, int> map_row_size = new Dictionary<int, int> { {1, 10 }, { 2, 10 }, { 3, 10 }, { 4, 12 }, { 5, 13 }, { 6, 16 } };
    Dictionary<int, float> map_row_cell = new Dictionary<int, float> { { 1, 59 }, { 2, 59 }, { 3, 59 }, { 4, 45.55f }, { 5, 43.04f }, { 6, 35.37f } };
    // Use this for initialization

	public void Reset()
	{
		foreach (var item in items) {
			Destroy (item.gameObject);
		}
		items.Clear ();
		cc.Clear ();
	}

    public void Init () {
		word = GameManager.instance._word;
        size = word.Length;
		//size=5;
        int r = 1;
        for (int i = 1; i < map_row_size.Count+1; i++)
        {
            var number = i * map_row_size[i];
            if(size<=number)
            {
                r = i;
                break;
            }
        }
        int length = r * map_row_size[r];
        if (r == 1)
            length = size;

		int dl = length - word.Length;

		for (int i = 0; i < dl; i++) {
			word+=" ";
		}

        int index = 0;
        for (int i = 0; i < length; i++)
        {
            var item = Instantiate(prefabs);
            item.transform.SetParent(transform);
            item.transform.localScale = new Vector3(1.0f, 1.0f, 1.0f);
            item.GetComponent<KeyControl>()._key = word[i];
            item.GetComponent<KeyControl>().index = index;
            items.Add(item);
            var text = item.transform.Find("Text");
            if(text!=null)
            {
				text.GetComponent<Text> ().text = "";//word[i].ToString();
                
               
            }
            if(word[i]==' ')
            {
                item.GetComponent<Image>().enabled = false;
                index++;
            }
        }

        var gridLayoutGroup = GetComponent<GridLayoutGroup>();
        gridLayoutGroup.cellSize = new Vector2(map_row_cell[r], map_row_cell[r]);
		cell = map_row_cell [r];
        gridLayoutGroup.constraintCount = r;



    }


	// Update is called once per frame
	void Update () {
		
	}

	public List<int> cc = new List<int> ();

    public void Show(int index)
    {
		if (!cc.Contains (index))
			cc.Add (index);
		
        foreach (var item in items)
        {
            if(item.GetComponent<KeyControl>().index==index)
            {
                item.GetComponent<KeyControl>()._show = true;
                var text = item.transform.Find("Text");
                if (text != null)
                {
                    text.GetComponent<Text>().text = item.GetComponent<KeyControl>()._key.ToString();


                }
            }
        }

		if (cc.Count == GameManager.instance._list.Count ) {
			GameManager.instance.ShowPass ();
		}
    }

	List<Vector2> list_pos = new List<Vector2> ();
	List<GameObject> _letters = new List<GameObject> ();
	int _id;
	int k=0;
	public void Move(List<GameObject> letters,int index,float cellsize)
	{
		_id = index;
		//_letters = letters;
		_letters.Clear();
		foreach (var item in letters) {
			_letters.Add (item);
		}
		//print (_letters.Count);
		list_pos.Clear ();
		k = 0;
		flag = false;
		if (!cc.Contains (index))
			cc.Add (index);
		List<GameObject> eats = new List<GameObject> ();
		foreach (var item in items)
		{
			if(item.GetComponent<KeyControl>().index==index)
			{
				item.GetComponent<KeyControl>()._show = true;
				eats.Add (item);
				list_pos.Add (item.transform.position);

			}
		}

		var scalesize = cell / cellsize;

		for (int i = 0; i < letters.Count-1; i++) {
			//letters [i].GetComponent<Button> ().interactable = true;
			//letters [i].transform.Find ("Text").GetComponent<Text> ().color = new Color (58.0f / 255.0f, 45.0f / 255.0f, 45.0f / 255.0f, 255);
			letters [i].GetComponent<Animator> ().enabled = false;
			letters [i].transform.DOScale (new Vector3 (scalesize, scalesize, 1), 1.0f);
			letters [i].transform.DOMove (eats [i].transform.position, 1.0f,false).OnComplete(()=>{
				Destroy(Instantiate(star, eats [k].transform.position, Quaternion.identity), 1.5f);
				//Destroy(letters[k].gameObject);
				_letters [k].GetComponent<Button> ().interactable = true;
				_letters [k].transform.Find ("Text").GetComponent<Text> ().color = new Color (58.0f / 255.0f, 45.0f / 255.0f, 45.0f / 255.0f, 255);
				k++;
			});


		}

		//letters [letters.Count-1].GetComponent<Button> ().interactable = true;
		//letters [letters.Count-1].transform.Find ("Text").GetComponent<Text> ().color = new Color (58.0f / 255.0f, 45.0f / 255.0f, 45.0f / 255.0f, 255);
		letters [letters.Count-1].GetComponent<Animator> ().enabled = false;
		letters [letters.Count-1].transform.DOScale (new Vector3 (scalesize, scalesize, 1), 1.0f);
		letters [letters.Count-1].transform.DOMove (eats [letters.Count-1].transform.position, 1.0f,false).OnComplete(()=>{
			Destroy(Instantiate(star, eats [k].transform.position, Quaternion.identity), 1.5f);
			//Destroy(letters[k].gameObject);
			_letters [k].GetComponent<Button> ().interactable = true;
			_letters [k].transform.Find ("Text").GetComponent<Text> ().color = new Color (58.0f / 255.0f, 45.0f / 255.0f, 45.0f / 255.0f, 255);
            if (GameManager.instance.onSound)
                GetComponent<AudioSource>().Play();
            Invoke("Go",1.5f);
		});

       
    }

	public void Go()
	{
       
		foreach (var item in items)
		{
			if(item.GetComponent<KeyControl>().index==_id)
			{
				item.GetComponent<KeyControl>()._show = true;
				var text = item.transform.Find("Text");
				if (text != null)
				{
					//text.GetComponent<Text>().text = item.GetComponent<KeyControl>()._key.ToString();


				}
			}
		}

		//print (_letters.Count);
		foreach (var item in _letters) {
			
			//item.SetActive (false);
		};
		print (_letters.Count);

		Invoke ("Move", 0.5f);

	}


	public void Move()
	{
		var go=GameObject.FindObjectOfType<BoardControl>();
		if(go!=null)
		{
			if (cc.Count == GameManager.instance._list.Count ) {
				flag=true;
			}
			go.GetComponent<BoardControl>().merge(flag);
		}
	}

	bool flag=false;


	public void Hint()
	{
		List<GameObject> list = new List<GameObject> ();
		List<int> indexs = new List<int> ();
		foreach (var item in items) {
			if (!item.GetComponent<KeyControl> ()._show) {
				string st = item.GetComponent<KeyControl> ()._key.ToString ();
				if(st.Equals(" ") || st.Equals(""))
					print (item.GetComponent<KeyControl> ().index+":"+st);
				if (!indexs.Contains (item.GetComponent<KeyControl> ().index) && !st.Equals("") && !st.Equals(" ")) {
					indexs.Add (item.GetComponent<KeyControl> ().index);

				}
			}
		}

		if (indexs.Count > 0) {
			var r = Random.Range (0, 100) % indexs.Count;
			var id = indexs [r];
			foreach (var item in items) {
				string st = item.GetComponent<KeyControl> ()._key.ToString ();
				if (!item.GetComponent<KeyControl> ()._show && item.GetComponent<KeyControl> ().index==id && !st.Equals("") && !st.Equals(" ") ) {
					list.Add (item);
				}
			}
			if (list.Count > 0) {
				var dr=0;//Random.Range (0, 100) % list.Count;
				var itemd=list[dr];
				itemd.GetComponent<KeyControl>()._show = true;
				var text = itemd.transform.Find("Text");
				if (text != null)
				{
					text.GetComponent<Text>().text = itemd.GetComponent<KeyControl>()._key.ToString();

					GameManager.instance.hints--;
					GameManager.instance.hint_used++;
					GameManager.instance.Save ();
				}
			}
		}


	}


}
