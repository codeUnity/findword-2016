﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class ListLevelControl : MonoBehaviour {

    public int count;
    public GameObject prefabs;
    List<GameObject> items = new List<GameObject>();
	// Use this for initialization

	public void Reset()
	{
		foreach (var item in items) {
			Destroy (item.gameObject);
		}
		items.Clear ();
		//Init ();
	}

	public void Init () {
		count = GameManager.instance.data.subjects [GameManager.instance._category-1].words.Count;
        for (int i = 0; i < count; i++)
        {
			//var subject = GameManager.instance.data.subjects [i];
			var word=GameManager.instance.data.subjects [GameManager.instance._category-1].words[i].ToUpper();
            var item = Instantiate(prefabs);
			item.GetComponent<ItemLevelControl> ().Init (i+1,word);
			item.transform.SetParent(transform.Find("Container").transform);
            item.transform.localScale = new Vector3(1.0f, 1.0f, 1.0f);
            items.Add(item);
        }

		int cc = GameManager.instance._max_level;
		if (GameManager.instance._max_category > GameManager.instance._category)
			cc = count;
		for (int i = 0; i < cc; i++) {
			items [i].transform.Find ("arrow").GetComponent<Button> ().interactable = true;
		}

    }
}
