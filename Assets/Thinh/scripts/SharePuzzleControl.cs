﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class SharePuzzleControl : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void ShareFacebook()
	{
        
        var code = GameObject.Find ("code");
		if (code) {
			var str = code.GetComponent<Text> ().text;
			FBServiceController.instance.ShareImage (str);
		}


	}

	const string Address = "http://twitter.com/intent/tweet";
	public void Sharetweet()
	{
       
        var code = GameObject.Find ("code");
		if (code) {
			var str = code.GetComponent<Text> ().text;
			Application.OpenURL(Address +
				"?text=" + WWW.EscapeURL("Hurraahh! CODE PUZZLE: "+ str )+
					" &amp;url=" + WWW.EscapeURL("\t") +
					"&amp;related=" + WWW.EscapeURL("\t") +
					"&amp;lang=" + WWW.EscapeURL("en"));
		}

	}

}
