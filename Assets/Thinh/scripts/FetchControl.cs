﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class FetchControl : MonoBehaviour {

	public GameObject input;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

	}

	public void Fetch()
	{
        var str = input.GetComponent<InputField> ().text;
		if(str.Length==6)
			GameManager.instance.FetchPuzzle (str);
	}

	public void Close()
	{
		GameManager.instance.ShowFetch(false);
	}
}
