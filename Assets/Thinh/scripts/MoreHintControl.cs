﻿using UnityEngine;
using System.Collections;
using CompleteProject;
using System.Collections.Generic;

public class MoreHintControl : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void Buy(int price)
    {
        IAPController.instance.GetComponent<Purchaser>().BuyConsumable(price-1);
      
    }

    public void CLose()
    {
        GameManager.instance.ShowMoreHint(false);
       
    }

    public void Share()
    {
        
        FBServiceController.instance.InviteFriend ();
    }

	public void Watch()
	{
       
#if UNITY_IOS
        Dictionary<string, object> options = new Dictionary<string, object>();
        options.Add("incentivized", true);
        options.Add("orientation", true);
        options.Add("large", true);
        options.Add("alertTitle", "Careful!");
        options.Add("alertText", "If the video isn't completed you won't get your reward! Are you sure you want to close early?");
        options.Add("closeText", "Close");
        options.Add("continueText", "Keep Watching");
        options.Add("immersive", true);

        Vungle.playAdWithOptions(options);

        //Vungle.playAd();

#else
        var options = new Dictionary<string, object> ();
		options ["incentivized"] = false;
		Vungle.playAdWithOptions (options);
#endif
	}

}
