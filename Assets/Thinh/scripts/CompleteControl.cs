﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class CompleteControl : MonoBehaviour {
	public List<Sprite> sprites;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void Init()
	{
		var value = GameManager.instance._size;
		if (GameManager.instance.statefetch == true)
			value = 2;
		var bonus= transform.Find ("bottom").Find ("bonus");
		if (bonus) {
			var str = string.Format ("BONUS: ({0})HINTS!", value);
			GameManager.instance.hints += value;
			bonus.GetComponent<Text> ().text = str;
		}
		//GameManager.instance.Save ();
		//GameManager.instance.Load ();
		var image=transform.Find ("image");
		if (image) {
			image.GetComponent<Image> ().sprite = sprites [GameManager.instance._category - 1];
		}
		GameManager.instance.SetWord ();
		GameManager.instance.Prepare ();
	}

	public void Next()
	{
        var go=GameObject.Find ("lower_unique");
		if (go) {
			go.GetComponent<LowerControl> ().Load ();
		}
		GameManager.instance.ReComplete ();
		GameManager.instance.Previous ();
	}

    public void Facebook()
    {
        FBServiceController.instance.ShareLink();
    }

    public void Twitter()
    {
        GAv3Control.instance.GetComponent<GoogleAnalyticsV3>()
    .LogEvent(new EventHitBuilder()
        .SetEventCategory("Twitter")
        .SetEventAction("Click"));
        Sharetweet();
    }


    const string Address = "http://twitter.com/intent/tweet";
    public void Sharetweet()
    {
        Application.OpenURL(Address +
                "?text=" + WWW.EscapeURL("Hurraahh! My Score in the #this game is  90 on Android https://play.google.com/store/apps/details?id=com.halfbrick.fruitninjafree") +
                "&amp;url=" + WWW.EscapeURL("\t") +
                "&amp;related=" + WWW.EscapeURL("\t") +
                "&amp;lang=" + WWW.EscapeURL("en"));
    }
}
