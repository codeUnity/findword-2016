﻿using UnityEngine;
using System.Collections;

public class IAPController : MonoBehaviour {

	private static IAPController _instance;
	
	public static IAPController instance {
		get {
			if (_instance == null) {
				_instance = GameObject.FindObjectOfType<IAPController> ();
				//Tell unity not 	to destroy this object when loading a new scene!
				//DontDestroyOnLoad (_instance.gameObject);
			}
			return _instance;
		}
	}

	private IAPController() {
	
	}
	
	void Awake ()
	{
		if (_instance == null) {
			//If I am the first instance, make me the Singleton
			_instance = this;
			//DontDestroyOnLoad (this);
		} else {
			//If a Singleton already exists and you find
			//another reference in scene, destroy it!
			if (this != _instance)
				Destroy (this.gameObject);
		}
	}
}
