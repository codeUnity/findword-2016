﻿using UnityEngine;
using System.Collections;
using GoogleMobileAds.Api;
using System;

public class AdmobServiceController : MonoBehaviour {

	public BannerView bannerView;
	private InterstitialAd interstitial;
    public AdPosition pos;
    public bool _showBanner = true;
    public bool _showFull = false;

    public int count=1;
    public int NumberShowFull=3;

    void OnDestroy()
    {
        //if(bannerView!=null)
        //bannerView.Destroy();
        //if(interstitial!=null)
        //interstitial.Destroy();
    }
    public void ShowInterstitial()
    {
		//count = PlayerPrefs.GetInt("full", 1);
		if (count % 3 == 0) {
			if(interstitial!=null)
			{
				if (interstitial.IsLoaded())
				{
					interstitial.Show();
				}
				else
				{
					print("Interstitial is not ready yet.");
				}
			}
		}
		count++;
		//PlayerPrefs.SetInt("full", count);
       
        
    }

	void Awake ()
	{
        if(GameManager.instance.show_ads)
        {
            if (_showBanner)
                RequestBanner();
            
            if(_showFull)
            {
                RequestFull();
            }
           

            
        }

        //print("Full" + PlayerPrefs.GetInt("full", 0));

    }

    public void RequestFull()
    {
        //count = PlayerPrefs.GetInt("full", 0);
        //if (count % 3 == 0)
            RequestInterstitial();
        //count++;
        //if (NumberShowFull != 0)
        //    count %= NumberShowFull;
        //PlayerPrefs.SetInt("full", count);
    }
	
	private void RequestBanner()
	{
		#if UNITY_EDITOR
		string adUnitId = "unused";
#elif UNITY_ANDROID
		string adUnitId = "ca-app-pub-7331094671419126/1322052090";
#elif UNITY_IPHONE
		string adUnitId = "INSERT_IOS_BANNER_AD_UNIT_ID_HERE";
#else
		string adUnitId = "unexpected_platform";
#endif

		// Create a 320x50 banner at the top of the screen.
		bannerView = new BannerView(adUnitId, AdSize.SmartBanner, pos);
		// Register for ad events.
		bannerView.AdLoaded += HandleAdLoaded;
		bannerView.AdFailedToLoad += HandleAdFailedToLoad;
		bannerView.AdOpened += HandleAdOpened;
		bannerView.AdClosing += HandleAdClosing;
		bannerView.AdClosed += HandleAdClosed;
		bannerView.AdLeftApplication += HandleAdLeftApplication;
		// Load a banner ad.
		bannerView.LoadAd(createAdRequest());
	}
	
	private void RequestInterstitial()
	{
		#if UNITY_EDITOR
		string adUnitId = "unused";
		#elif UNITY_ANDROID
		string adUnitId = "ca-app-pub-7882417454111138/2605705605";
#elif UNITY_IPHONE
		string adUnitId = "ca-app-pub-7882417454111138/5698772808";
#else
		string adUnitId = "unexpected_platform";
#endif

        // Create an interstitial.
		interstitial = new InterstitialAd(adUnitId);
		// Register for ad events.
		interstitial.AdLoaded += HandleInterstitialLoaded;
		interstitial.AdFailedToLoad += HandleInterstitialFailedToLoad;
		interstitial.AdOpened += HandleInterstitialOpened;
		interstitial.AdClosing += HandleInterstitialClosing;
		interstitial.AdClosed += HandleInterstitialClosed;
		interstitial.AdLeftApplication += HandleInterstitialLeftApplication;
		// Load an interstitial ad.
		interstitial.LoadAd(createAdRequest());
	}
	
	// Returns an ad request with custom ad targeting.
	private AdRequest createAdRequest()
	{
		return new AdRequest.Builder()
			.AddTestDevice(AdRequest.TestDeviceSimulator)
				.AddTestDevice("7BD486AB8AD1D463DD2807A1FB93A200")
				.AddKeyword("game")
				.SetGender(Gender.Male)
				.SetBirthday(new DateTime(1985, 1, 1))
				.TagForChildDirectedTreatment(false)
				.AddExtra("color_bg", "9B30FF")
				.Build();
		
	}
	
	
	#region Banner callback handlers
	
	public void HandleAdLoaded(object sender, EventArgs args)
	{
		print("HandleAdLoaded event received.");
	}
	
	public void HandleAdFailedToLoad(object sender, AdFailedToLoadEventArgs args)
	{
		print("HandleFailedToReceiveAd event received with message: " + args.Message);
	}
	
	public void HandleAdOpened(object sender, EventArgs args)
	{
		print("HandleAdOpened event received");
	}
	
	void HandleAdClosing(object sender, EventArgs args)
	{
		print("HandleAdClosing event received");
	}
	
	public void HandleAdClosed(object sender, EventArgs args)
	{
		print("HandleAdClosed event received");
	}
	
	public void HandleAdLeftApplication(object sender, EventArgs args)
	{
		print("HandleAdLeftApplication event received");
	}
	
	#endregion
	
	#region Interstitial callback handlers
	
	public void HandleInterstitialLoaded(object sender, EventArgs args)
	{
		print("HandleInterstitialLoaded event received.");
        //ShowInterstitial();
	}
	
	public void HandleInterstitialFailedToLoad(object sender, AdFailedToLoadEventArgs args)
	{
		print("HandleInterstitialFailedToLoad event received with message: " + args.Message);
	}
	
	public void HandleInterstitialOpened(object sender, EventArgs args)
	{
		print("HandleInterstitialOpened event received");
	}
	
	void HandleInterstitialClosing(object sender, EventArgs args)
	{
		print("HandleInterstitialClosing event received");
       

    }
	
	public void HandleInterstitialClosed(object sender, EventArgs args)
	{
		print("HandleInterstitialClosed event received");
		Invoke ("Load", 0.5f);
        
    }
	
	public void HandleInterstitialLeftApplication(object sender, EventArgs args)
	{
		print("HandleInterstitialLeftApplication event received");
	}

	public void Load()
	{
		RequestInterstitial ();
	}
	
	#endregion
}
