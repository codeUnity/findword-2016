﻿using UnityEngine;
using System.Collections;
using Facebook.Unity;
using System.Collections.Generic;
using System;

public class FBServiceController : MonoBehaviour
{

	private static FBServiceController _instance;

	public static FBServiceController instance {
		get {
			if (_instance == null) {
				_instance = GameObject.FindObjectOfType<FBServiceController> ();
				//Tell unity not 	to destroy this object when loading a new scene!
				DontDestroyOnLoad (_instance.gameObject);
			}
			return _instance;
		}
	}
	
	void Awake ()
	{
		if (_instance == null) {
			//If I am the first instance, make me the Singleton
			_instance = this;
			DontDestroyOnLoad (this);
		} else {
			//If a Singleton already exists and you find
			//another reference in scene, destroy it!
			if (this != _instance)
				Destroy (this.gameObject);
		}
	}

	// Use this for initialization
	void Start ()
	{
		FB.Init ();
	}
	
	// Update is called once per frame
	void Update ()
	{
	
	}

	public void Login ()
	{
		FB.LogInWithReadPermissions (new List<string> () { "public_profile", "email", "user_friends" }, (ILoginResult result) => {
			ShareLink ();
		});
	}
	
	public void ShareLink ()
	{
#if UNITY_ANDROID
        FB.ShareLink (new Uri ("market://details?id=com.pixa.findword"), "", "", null, (IShareResult result) => {

        });
#elif UNITY_IOS
         FB.ShareLink (new Uri ("itms-apps://itunes.apple.com/app/1074674966"), "", "", null, (IShareResult result) => {
			
		});
#endif
    }


 //   public void FeedShare(string value)
	//{
	//	FB.FeedShare(
	//		string.Empty,
	//		new Uri ("https://www.facebook.com/games/com_pixa_findword/?fbs=-1&preview=1"),
	//		"Puzzle Code",
	//		"Puzzle Code: "+value,
	//		"Puzzle Code: "+value,
	//		null,
	//		string.Empty,(IShareResult result)=>{
	//			print(result.ToString());
	//			if (!GameManager.instance._stateplay) {
	//				GameManager.instance.Previous (2);
	//				GameManager.instance.BackIdle ();
	//				GameManager.instance.ShowMenu ();
	//				return;
	//			}
	//		} );
	//}

	public void ShareImage(String value)
	{
#if UNITY_ANDROID
        FB.ShareLink (new Uri ("market://details?id=com.pixa.findword"), "Puzzle Code", "Puzzle Code: "+value, null, (IShareResult result) => {
            print(result.ToString());
			if (!GameManager.instance._stateplay) {
				GameManager.instance.Previous (2);
				GameManager.instance.BackIdle ();
				GameManager.instance.ShowMenu ();
				return;
			}
		});
#elif UNITY_IOS
        FB.ShareLink (new Uri ("itms-apps://itunes.apple.com/app/1074674966"), "Puzzle Code", "Puzzle Code: "+value, null, (IShareResult result) => {
			print(result.ToString());
			if (!GameManager.instance._stateplay) {
				GameManager.instance.Previous (2);
				GameManager.instance.BackIdle ();
				GameManager.instance.ShowMenu ();
				return;
			}
		});
#endif
        //		if (FB.IsLoggedIn)
        //			StartCoroutine (ShareCapture ());
        //		else
        //			LoginShare ();	
    }

    //	public void LoginShare()
    //	{
    //		FB.LogInWithReadPermissions (new List<string> () { "public_profile","publish_actions", "email", "user_friends" }, (ILoginResult result) => {
    //			ShareImage();
    //		});
    //	}
    //
    //	IEnumerator ShareCapture()
    //	{
    //		yield return new WaitForEndOfFrame ();
    //		var snap = new Texture2D(Screen.width, Screen.height, TextureFormat.RGB24, false);
    //		snap.ReadPixels(new Rect(0, 0, Screen.width, Screen.height), 0, 0);
    //		snap.Apply();
    //		var screenshot = snap.EncodeToPNG();
    //		var wwwForm = new WWWForm();
    //		wwwForm.AddBinaryData("image", screenshot, "picture.png");
    //		wwwForm.AddField ("name", "CODE TO GET PUZZLE!");
    //
    //		FB.API("me/photos", HttpMethod.POST,(IGraphResult result)=>{
    //			print(result.ToString());
    //			if (!GameManager.instance._stateplay) {
    //				GameManager.instance.Previous (2);
    //				GameManager.instance.BackIdle ();
    //				GameManager.instance.ShowMenu ();
    //				return;
    //			}
    //		}, wwwForm);
    //	}

    public void InviteFriend()
    {
#if UNITY_ANDROID
		FB.Mobile.AppInvite (new Uri ("market://details?id=com.pixa.findword"), callback: (IAppInviteResult result) => {
            GameManager.instance.hints += 5;
            GameManager.instance.Save();
            GameManager.instance.ShowMoreHint(false);
            GameManager.instance.ShowMenu();
        });
	
#elif UNITY_IOS
		FB.Mobile.AppInvite (new Uri ("itms-apps://itunes.apple.com/app/1074674966"), callback: (IAppInviteResult result) => {
            GameManager.instance.hints += 5;
            GameManager.instance.Save();
            GameManager.instance.ShowMoreHint(false);
            GameManager.instance.ShowMenu();
        });
#endif
    }
	
	public void ShowUser ()
	{
		List<object> filter = new List<object> () { "app_users" };
		
		// workaround for mono failing with named parameters
		FB.AppRequest ("ZBert", null, filter, null, 0, string.Empty, string.Empty, (IAppRequestResult result) => {
			
		});
	}
}
