﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class ItemLevelControl : ItemControl {

    public List<Sprite> sprites;
    public int level;
    public int hint;
    public bool solve = false;
	public string _word;

	public void Click()
	{
        if (level <= GameManager.instance._max_level|| GameManager.instance._max_category>GameManager.instance._category) {
			//print (level);
			GameManager.instance._word = _word;
			GameManager.instance._level = level;
			GameManager.instance.Prepare ();
			GameManager.instance.Next ();
			GameManager.instance.ResetGame ();
			Invoke ("ShowGame",0.5f);


		}
	}

	public void ShowGame ()
	{
		GameManager.instance.ShowGame ();
	}

	public void Init(int level,string word)
	{
		this.level = level;
		this._word = word;
		var key = GameManager.instance.data.subjects [GameManager.instance._category - 1].name + level;
		hint = PlayerPrefs.GetInt (key, 0);
		solve=PlayerPrefs.GetInt (key+"solve", 0)==1?true:false;
		Show ();
	}

	public override void Show()
    {
        var res = transform.Find("res");
        var txt_level_number = transform.Find("txt_level_number");
        var txt_solve = transform.Find("txt_solve");
        var txt_hint_number = transform.Find("txt_hint_number");
        var arrow = transform.Find("arrow");

        
        if (res != null)
        {
            var r = Random.Range(0, sprites.Count - 1);
            res.GetComponent<Image>().sprite = sprites[r];
        }
        if (txt_level_number != null)
        {
            txt_level_number.GetComponent<Text>().text =level.ToString();
        }

        if (txt_hint_number != null)
        {
            if(hint>0 && solve)
                txt_hint_number.GetComponent<Text>().text = hint.ToString() + " HINTS USED";
            else
                txt_hint_number.GetComponent<Text>().text = "";
        }

        if (txt_solve != null)
        {
            if(solve)
                txt_solve.GetComponent<Text>().text = "SOLVED";
            else
                txt_solve.GetComponent<Text>().text = "NOT SOLVE";
        }

        if(arrow!=null)
        {
            //print("arrow");
            arrow.GetComponent<Button>().interactable = solve;
        }
    }
}
