﻿using UnityEngine;
using System.Collections;

public class BackControl : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void Click()
	{
        if (!GameManager.instance.can_back)
            return;
		switch (GameManager.instance._current) {
		case Layer.Category:
			GameManager.instance._backing = true;
		GameManager.instance.BackIdle ();
		GameManager.instance.ShowMenu ();


			break;
		case Layer.Level:
			if (!GameManager.instance._stateplay) {
				GameManager.instance.Previous (2);
				GameManager.instance.BackIdle ();
				GameManager.instance.ShowMenu ();
				return;
			}else
			GameManager.instance.ShowCategory (GameManager.instance._stateplay);
			break;
		case Layer.Game:
			GameManager.instance.ShowLevel (GameManager.instance._stateplay);
			break;
		case Layer.Complete:
			//GameManager.instance.ReComplete ();
			GameManager.instance.Previous (3);
			GameManager.instance.ShowCategory (GameManager.instance._stateplay);
			return;
		default:
			break;
		}
		GameManager.instance.Previous ();
	}
}
