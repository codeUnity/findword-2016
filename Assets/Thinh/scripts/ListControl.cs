﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class ListControl : MonoBehaviour {

    public int count;
    public GameObject prefabs;
    List<GameObject> items = new List<GameObject>();
	// Use this for initialization

	public void Reset()
	{
		foreach (var item in items) {
			Destroy (item.gameObject);
		}
		items.Clear ();
		//print (items);
	}


	public void Init () {
		count = GameManager.instance.data.subjects.Count;
        for (int i = 0; i < count; i++)
        {
			var subject = GameManager.instance.data.subjects [i];
            var item = Instantiate(prefabs);
			item.GetComponent<ItemCategoryControl> ().Init (subject.name, subject.size, subject.brain,i+1);
			item.transform.SetParent(transform.Find("Container").transform);
            item.transform.localScale = new Vector3(1.0f, 1.0f, 1.0f);
            items.Add(item);
        }
		for (int i = 0; i < GameManager.instance._max_category; i++) {
			items [i].transform.Find ("arrow").GetComponent<Button> ().interactable = true;
		}

    }

}
