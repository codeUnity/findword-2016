﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CreateControl : MonoBehaviour {

	public GameObject notify;
	public GameObject input;
	public GameObject create;
	public GameObject title;
	public GameObject note;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}


	public void Init()
	{
		GameManager.instance._backing = false;
		title.SetActive (true);
		note.SetActive (true);
		create.SetActive (false);
		if (input)
			input.GetComponent<InputField> ().text = "";
	}


	public void ValueChange()
	{
		//var input=transform.Find ("InputField");
		if (input != null) {
			var str=input.GetComponent<InputField> ().text;
			//if (str.Length == 1)
			//if (str [0] == ' ')
			//	input.GetComponent<InputField> ().text = "";
			if (str.Length >= 2) {
				if (str [str.Length - 1] == ' ' && str [str.Length - 2] == ' ')
					str = str.Substring (0, str.Length - 1);
			
				if (str [str.Length - 1] == ' ') {
					string temp = str.Substring (0, str.Length - 1);
					var index = temp.LastIndexOf (' ');
					int length = 0;
					if (index <= 0) {
						index = 0;
						length = temp.Substring (index, temp.Length - index).Length;
						print ("Length" + length);
					} else {
						length = temp.Substring (index, temp.Length - index).Length-1;
						print ("Length" + length);
					}
					if (length >= 10) {
						notify.GetComponent<Text> ().text = "NO WORDS GREATER THAN 10 LETTERS!";
					} else if (length < 2) {
						notify.GetComponent<Text> ().text = "NO WORDS LESS THAN 2 LETTERS!";
					} else {
						notify.GetComponent<Text> ().text = "";
					}

				}
			}
//			input.GetComponent<InputField> ().text = str;
//			input.GetComponent<InputField> ().text = str.ToUpper ();
//			var list = str.Split (' ');
//			foreach (var item in list) {
//				print (item);
//				if (item.Length >= 10) {
//					notify.GetComponent<Text>().text="NO WORDS 10 LETTERS!";
//					break;
//				}
//				if (item.Length == 1 && list.Length >= 2)
//				if (item [0] != ' ') {
//					notify.GetComponent<Text> ().text = "NO WORDS LESS THAN 2 LETTERS!";
//					print (item);
//				}
//			}

			input.GetComponent<InputField> ().text = str.ToUpper().TrimStart ();
//			if (str.Length < 2) {
//				notify.GetComponent<Text>().text="NO WORDS LESS THAN 2 LETTERS!";
//			}
			if(str.Length>=1)
			create.SetActive(true);
			title.SetActive (false);
			note.SetActive (false);
			var ss=str.Replace(" ","").ToUpper();
			create.GetComponent<BoardCreateControl> ().Reset (ss);
			create.GetComponent<BoardCreateControl> ().Init ();


		}
	}
	string tg="";

	public void EndEdit()
	{
		print ("EndEdit");
		var str=input.GetComponent<InputField> ().text;
		var list = str.Split (' ');
		var flag = false;
		foreach (var item in list) {
			print (item);
			if (item.Length >= 10) {
				notify.GetComponent<Text>().text="NO WORDS 10 LETTERS!";
				flag = true;
				break;
			}
			if (item.Length == 1 && list.Length >= 2)
			if (item [0] != ' ') {
				flag = true;
				notify.GetComponent<Text> ().text = "NO WORDS LESS THAN 2 LETTERS!";
				print (item);
				break;
			}
		}
		create.SetActive (false);
		tg = str.ToUpper ();
		if (!flag) {
			input.GetComponent<InputField> ().text = "";
			Invoke ("Post", 0.5f);
		}


	}
	public void Post()
	{
		if (input != null && !GameManager.instance._backing) {
			print (input.GetComponent<InputField> ().text);
			if (tg.Length >= 2)
				GameManager.instance.PostPuzzle (tg);
			else {
				input.GetComponent<InputField> ().text = tg.ToString ();
				notify.GetComponent<Text> ().text = "NO WORDS LESS THAN 2 LETTERS!";
			}

		}
	}
}
