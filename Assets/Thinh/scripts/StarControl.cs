﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class StarControl : MonoBehaviour {

	public GameObject fan;
	public GameObject brand;
	public GameObject logo;
	public GameObject brain_number;

	public void Move()
	{
		logo.SetActive (false);
		brain_number.GetComponent<Text> ().text = "";
		GetComponent<Animator> ().SetTrigger ("move");
	}

	public void Normal()
	{
		brain_number.GetComponent<Text> ().text = "";
		fan.SetActive (false);
		brand.SetActive (false);
		GetComponent<Animator> ().SetTrigger ("normal");
	}

	public void ReShow()
	{
		logo.SetActive (true);
		GameManager.instance.ReShowGame ();
	}

	public void Run()
	{
		fan.SetActive (true);
		brand.SetActive (true);
		brain_number.GetComponent<Text> ().text = GameManager.instance.brain_size.ToString();
		Invoke ("Normal", 1.0f);
	}
}
