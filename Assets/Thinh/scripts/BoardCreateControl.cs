﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using System.Text;
using DG.Tweening;

public class BoardCreateControl : MonoBehaviour
{

	public int size;
	public string word = "Hello";
	public GameObject prefabs;
	List<GameObject> items = new List<GameObject> ();
	Dictionary<int, int> map_row_spacing = new Dictionary<int, int> {
		{ 2, 30 }, { 3, 20 }, { 4, 18 }, { 5, 15 }, { 6,12 }, { 7,10 }, { 8,8 }
	};
	Dictionary<int, int> map_row_font = new Dictionary<int, int> {
		{ 2, 180 }, { 3, 100 }, { 4, 80 }, { 5, 64 }, { 6,56 }, { 7,48 }, { 8,40 }
	};

	public void Reset()
	{
		foreach (var item in items) {
			Destroy (item.gameObject);
		}
		items.Clear ();
		GetComponent<GridLayoutGroup> ().enabled=true;
		float sqrt = Mathf.Sqrt(word.Length);
		int _size = Mathf.RoundToInt (sqrt);
	
		if (_size*_size<word.Length)
			_size++;
		size = _size;

	}

	public void Reset(string value)
	{
		word = value;
		List<char> list = new List<char> ();
		for (int i = 0; i < value.Length; i++) {
			list.Add (value [i]);
		}
		list.Sort (delegate(char x, char y) {
			int r=Random.Range(-1,1);
			return r;
		});

		StringBuilder strs = new StringBuilder ();
		for (int i = 0; i < list.Count; i++) {
			if (word [i] != ' ') {

				strs.Append (list [i]);
			}
		}
		word = strs.ToString ();


		foreach (var item in items) {
			Destroy (item.gameObject);
		}
		items.Clear ();
		GetComponent<GridLayoutGroup> ().enabled=true;
		float sqrt = Mathf.Sqrt(word.Length);
		int _size = Mathf.RoundToInt (sqrt);

		if (_size*_size<word.Length)
			_size++;
		size = _size;
		if (size < 2)
			size = 2;

	}
    

	// Use this for initialization
	public void Init ()
	{
		if (size >= 8)
			return;
		//return;
		var gridLayoutGroup = GetComponent<GridLayoutGroup> ();
		rt=GetComponent<RectTransform> ();
		//rt.anchoredPosition = new Vector2 (-320+12.5f, rt.anchoredPosition.y);
		var length = (560.0f - (18 - 1.5f * size) * (size - 1)) / size;
		gridLayoutGroup.cellSize = new Vector2 (length, length);
		gridLayoutGroup.spacing = new Vector2 (map_row_spacing [size], map_row_spacing [size]);
		gridLayoutGroup.constraintCount = size;
		//for (int i = 0; i < 5; i++) {
		for (int i = 0; i < size * size; i++) {
			var item = Instantiate (prefabs);
			item.transform.SetParent (transform);
			item.transform.localScale = new Vector3 (1.0f, 1.0f, 1.0f);
			item.GetComponent<LetterControl> ().location = new Vector2 (i / size, i % size);
			items.Add (item);
			//array_item [i / size, i % size] = item;
			//array_postion [i / size, i % size] = item.transform.position;
			//item.GetComponent<Image> ().enabled = false;
			//item.GetComponent<BoxCollider2D> ().size = new Vector2 (length-2, length-2);
			var text = item.transform.Find ("Text");
			if (text != null) {
				text.GetComponent<Text> ().fontSize = map_row_font [size];
				text.GetComponent<Text> ().enabled = false;
				if (i < word.Length) {
					text.GetComponent<Text> ().text = word [i].ToString ();
					text.GetComponent<Text> ().enabled = true;
				}
			}
		}
    	
		if (!flag) {
			flag = true;
			rt=GetComponent<RectTransform> ();
			sizedelta = rt.sizeDelta;
			//print (sizedelta);
			rt.anchoredPosition += new Vector2 (sizedelta.y / 2.0f, 0);
		}

	}
	bool flag=false;
	RectTransform rt;
	Vector2 sizedelta;
}
