﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using System.Text;
using DG.Tweening;

public class BoardControl : MonoBehaviour
{

	public int size;
	public string word = "";
	public GameObject prefabs;
	List<GameObject> items = new List<GameObject> ();
	Dictionary<Vector2, char> map_char_location = new Dictionary<Vector2,char > ();
	List<Vector2> list_location = new List<Vector2> ();
	List<Vector2> list_location_temp = new List<Vector2> ();
	List<Vector2> list_sort = new List<Vector2> ();
	Dictionary<int, int> map_row_spacing = new Dictionary<int, int> {
		{ 2, 30 }, { 3, 20 }, { 4, 18 }, { 5, 10 }, { 6,12 }, { 7,10 }, { 8,8 }
	};
	Dictionary<int, int> map_row_font = new Dictionary<int, int> {
		{ 2, 180 }, { 3, 100 }, { 4, 80 }, { 5, 64 }, { 6,56 }, { 7,48 }, { 8,40 }
	};
	public int _index;
	float cellsize;
	bool[,] array;
	//GameObject[,] array_item;

	public void Reset()
	{
        GameManager.instance.can_back = true;
        foreach (var item in items) {
			Destroy (item.gameObject);
		}
		items.Clear ();
		_currents.Clear ();
		map_char_location.Clear ();
		list_location.Clear ();
		list_location_temp.Clear ();
		list_sort.Clear ();

		GetComponent<GridLayoutGroup> ().enabled=true;

	}
    

	// Use this for initialization
	public void Init ()
	{
        GameManager.instance.can_back = true;
		//GameManager.instance.hint_used = 0;
		size = GameManager.instance._size;
		if (size <= 2)
			size = 2;
		array = new bool[size, size];
		//array_item = new GameObject[size, size];
		array_postion = new Vector2[size, size];

		List<Vector2> tp = new List<Vector2> ();
		for (int nx = 0; nx < size; nx++) {
			for (int ny = 0; ny < size; ny++) {
				var vt = new Vector2 (nx, ny);

				tp.Add (vt);
				array [nx, ny] = false;
			}
		}
		//print ("Pass");
		do {
			var r = Random.Range (0, 100) % tp.Count;
			list_sort.Add (tp [r]);
			//print (tp [r]);
			tp.RemoveAt (r);
		} while(tp.Count > 0);

		//print ("Pass");
		StringBuilder str = new StringBuilder ();
		word = GameManager.instance._word.Trim ();

		//print ("New" + word);
		for (int i = 0; i < word.Length; i++) {
			if (word [i] != ' ') {

				str.Append (word [i]);
			}
		}
		word = str.ToString ();
		print (word);
//		int ki = 0;
//		do {
//			var dx = Random.Range (0, 100)%size;
//			var dy = Random.Range (0, 100)%size;
//			temp = new Vector2 (dx, dy);
//			fill ();
//			ki++;
//		} while(!check () && ki <= 10);
		//list_sort[0]=new Vector2(2,1);
		foreach (var tm in list_sort) {
			temp = tm;
			fill ();
			//print ("TM:" + tm);
			if (check ())
				break;
		}


		var gridLayoutGroup = GetComponent<GridLayoutGroup> ();
		rt=GetComponent<RectTransform> ();
		//rt.anchoredPosition = new Vector2 (-320+12.5f, rt.anchoredPosition.y);
		var length = (560.0f - (18 - 1.5f * size) * (size - 1)) / size;
		cellsize = length;
		gridLayoutGroup.cellSize = new Vector2 (length, length);
		gridLayoutGroup.spacing = new Vector2 (map_row_spacing [size], map_row_spacing [size]);
		gridLayoutGroup.constraintCount = size;
		//for (int i = 0; i < 5; i++) {
		for (int i = 0; i < size * size; i++) {
			var item = Instantiate (prefabs);
			item.transform.SetParent (transform);
			item.transform.localScale = new Vector3 (1.0f, 1.0f, 1.0f);
			item.GetComponent<LetterControl> ().location = new Vector2 (i / size, i % size);
			items.Add (item);
			//array_item [i / size, i % size] = item;
			//array_postion [i / size, i % size] = item.transform.position;
			item.GetComponent<Image> ().enabled = false;
			var text = item.transform.Find ("Text");
			if (text != null) {
				//if (i <= word.Length && map_char_location.ContainsKey (new Vector2 (i / size, i % size)))
				if (map_char_location.ContainsKey (new Vector2 (i / size, i % size)))
				{
					text.GetComponent<Text> ().text = map_char_location [new Vector2 (i / size, i % size)].ToString ();//word[i].ToString();
					print(text.GetComponent<Text> ().text);
				}
				text.GetComponent<Text> ().fontSize = map_row_font [size];
				text.GetComponent<Text> ().enabled = false;
			}
			item.GetComponent<BoxCollider2D> ().size = new Vector2 (length-2, length-2);
		}
    	
		if (!flag) {
			flag = true;
			rt=GetComponent<RectTransform> ();
			sizedelta = rt.sizeDelta;
			//print (sizedelta);
			rt.anchoredPosition += new Vector2 (sizedelta.y / 2.0f, 0);
		}


		StopAllCoroutines ();
		StartCoroutine (Show ());

        var admob = GameObject.Find("AdmobService");
        if(admob!=null)
        {
            admob.GetComponent<AdmobServiceController>().ShowInterstitial();
        }

	}
	bool flag=false;
	RectTransform rt;
	Vector2 sizedelta;
	IEnumerator Show()
	{
		yield return new WaitForSeconds (0.1f);
		//print ("Show");
		foreach (var item in items) {
			var text = item.transform.Find ("Text");
			var str = text.GetComponent<Text> ().text;
			var location = item.GetComponent<LetterControl> ().location;
			if (!str.Equals ("1")) {
				item.GetComponent<Image> ().enabled = true;
				item.GetComponent<Animator> ().enabled = true;
				text.GetComponent<Text> ().enabled = true;
				item.GetComponent<BoxCollider2D> ().enabled = true;

				array [(int)location.x, (int)location.y] = false;
				item.GetComponent<LetterControl> ()._answer = false;
			} else {
				array [(int)location.x, (int)location.y] = true;
				item.GetComponent<LetterControl> ()._answer = true;
				item.GetComponent<BoxCollider2D> ().enabled = false;
			}
			yield return new WaitForSeconds (0.1f);
		}
		//yield return new WaitForSeconds (0.1f);
		GetComponent<GridLayoutGroup> ().enabled=false;
		//print (sizedelta);

		for (int i = 0; i < items.Count; i++) {
			array_postion [i / size, i % size] = items [i].transform.position;
		}
		merge (false);


	}
		

  
	List<GameObject> _currents = new List<GameObject> ();
	// Update is called once per frame
	GameObject before;

	void Update ()
	{
		if (GameManager.instance.show_instructions)
			return;
		if (Input.GetMouseButton (0)) {
			RaycastHit2D info = Physics2D.Raycast (Camera.main.ScreenToWorldPoint (Input.mousePosition), Vector2.zero);
			if (info) {
				var obj = info.transform.gameObject;
				if (obj.tag == "letter") {
					if (!_currents.Contains (obj) && !obj.GetComponent<LetterControl> ()._answer) {
						if (before != null) {
							var neighbors = before.GetComponent<LetterControl> ().neighbors ();
							if (neighbors.Contains (obj.GetComponent<LetterControl> ().location)) {
								_currents.Add (obj);
								before = obj;
								obj.GetComponent<Button> ().interactable = false;
								obj.transform.Find ("Text").GetComponent<Text> ().color = Color.green;
                                if(GameManager.instance.onSound)
								    obj.GetComponent<AudioSource> ().Play ();
							}
						} else {
                           
							_currents.Add (obj);
                           
							before = _currents [_currents.Count - 1];
							//print (before.GetComponent<LetterControl> ().location);
							obj.GetComponent<Button> ().interactable = false;
							obj.transform.Find ("Text").GetComponent<Text> ().color = Color.green;
                            if (GameManager.instance.onSound)
                                obj.GetComponent<AudioSource>().Play();
                        }
                        
					}
                    
                    
                    
				}
				string str = "";
				foreach (var item in _currents) {
					str += item.transform.Find ("Text").GetComponent<Text> ().text;
				}

				GameObject.Find ("upper").GetComponent<Text> ().text = str;
               

			}
		}
		if (Input.GetMouseButtonUp (0)) {
          
            if (GameManager.instance._list_temp.Contains (GameObject.Find ("upper").GetComponent<Text> ().text)) {
                if (GameManager.instance._current == Layer.Game)
                    GameManager.instance.can_back = false;
                _index = GameManager.instance._list_temp.IndexOf (GameObject.Find ("upper").GetComponent<Text> ().text);
//				print ("Index" + _index);
				foreach (var item in _currents) {
					//item.transform.Find ("Text").GetComponent<Text> ().color = Color.green;
					item.GetComponent<LetterControl> ()._answer = true;
					var location = item.GetComponent<LetterControl> ().location;
					array [(int)location.x, (int)location.y] = true;
					array_postion [(int)location.x, (int)location.y] = item.transform.position;
				}
				var go = GameObject.Find ("answer");
				//go.GetComponent<AnswerControl> ().Show (_index);
				go.GetComponent<AnswerControl> ().Move(_currents,_index,cellsize);
				//merge ();
			}else
			foreach (var item in items) {
				if (!item.GetComponent<LetterControl> ()._answer) {
					item.GetComponent<Button> ().interactable = true;
					item.transform.Find ("Text").GetComponent<Text> ().color = new Color (58.0f / 255.0f, 45.0f / 255.0f, 45.0f / 255.0f, 255);
				}
                
			}
			_currents.Clear ();
			before = null;
			var o = GameObject.Find ("upper");
			if (o != null)
				o.GetComponent<Text> ().text = "";
		}
	}

	Vector2[,] array_postion;
	Dictionary<Vector2,Vector2> map_merge=new Dictionary<Vector2, Vector2>();
	public void merge(bool flag)
	{
		map_merge.Clear ();
		string str = "";
		for (int i = 0; i < size; i++) {
			for (int j = 0; j < size; j++) {
				str+=(array[i,j]+" ");
			}
			str+= ("\n");
		}
		print ("str1: " + str);

		for (int i = size-2; i >= 0; i--) {
			for (int j = 0; j < size; j++) {
				if (array [i, j] == false) {
					for (int k = size-1; k >= i; k--) {
						if (array [k, j] == true) {
							array [k, j] = false;
							array [i, j] = true;
							map_merge [new Vector2 (i, j)] = new Vector2 (k, j);
							break;
						}
					}
				}
			}
		}	

		//print ("2,0: "+array_postion [2, 0]);




//		for (int i = 0; i < size; i++) {
//			for (int j = 0; j < size; j++) {
//				var item = array_item [i, j];
//
//				if (!item.GetComponent<LetterControl> ()._answer) {
//					var location = item.GetComponent<LetterControl> ().location;
//					var des = map_merge [location];
//					print (location.ToString () + ":" + des.ToString ());
//					item.transform.DOMove (new Vector3 (array_item [(int)des.x, (int)des.y].transform.position.x, array_item [(int)des.x, (int)des.y].transform.position.y, 0), 2.0f, false);
//				}
//			}
//
//		}
		StopAllCoroutines();
		StartCoroutine(Sort(flag));
		str = "";
		for (int i = 0; i < size; i++) {
			for (int j = 0; j < size; j++) {
				str+=(array[i,j]+" ");
			}
			str+= ("\n");
		}
		print ("str2: " + str);
	}


	IEnumerator Sort(bool flag)
	{
		yield return new WaitForEndOfFrame ();
		for (int i = 0; i < items.Count; i++) {
			var item = items [i];
			if (!item.GetComponent<LetterControl> ()._answer) {
				var location = item.GetComponent<LetterControl> ().location;

				if (map_merge.ContainsKey (location)) {
					var des = map_merge [location];
					//print (location.ToString () + ":" + des.ToString ());
					item.transform.DOMove (new Vector3 (array_postion [(int)des.x, (int)des.y].x, array_postion [(int)des.x, (int)des.y].y, 0), 0.75f, false);
					item.GetComponent<LetterControl> ().location = des;
				}
			}
		}
        GameManager.instance.can_back = true;
		if (flag) {
            GameManager.instance.can_back = false;
            GameManager.instance.ShowPass ();
		}
	}

	Vector2 temp;

	public void fill ()
	{
		list_location.Clear ();
		map_char_location.Clear ();
		for (int ik = 0; ik < GameManager.instance._list.Count; ik++) {
			string str = GameManager.instance._list [ik];
			map_char_location [temp] = str [0];
			list_location.Add (temp);
			list_location_temp.Clear ();
			list_location_temp.Add (temp);
			for (int i = 1; i < str.Length; i++) {
				var neighbors = LetterControl.neighbors (list_location_temp [i - 1], size);
				bool flag = false;
				foreach (var item in neighbors) {
                  
					if (!list_location.Contains (item)) {
						list_location.Add (item);
						list_location_temp.Add (item);
						map_char_location [item] = str [i];

						if (i == str.Length - 1) {
							var t = temp;
							var ns = LetterControl.neighbors (item, size);
							foreach (var n in ns) {
								if (!list_location.Contains (n)) {
									temp = n;
									break;
								} 
							}
							if (t.Equals (temp))
								return;
								
							
						}
						flag = true;
						break;
					}

				}
				if (!flag)
					return;
			}
		}



	}


	public bool check ()
	{
		for (int nx = 0; nx < size; nx++) {
			for (int ny = 0; ny < size; ny++) {
				var vt = new Vector2 (nx, ny);
				if (!map_char_location.ContainsKey (vt)) {
					return false;
				}
			}
		}
		return true;

//		for (int i = 0; i < word.Length; i++) {
//			char ch = word [i];
//			if (!map_char_location.ContainsValue (ch)) {
//				return false;
//			}
//		}
//		return true;
	}
}
