﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class SettingControl : MonoBehaviour {

	// Use this for initialization
	public void Init () {
        var go = GameObject.Find("ToggleSound");
        if (go)
        {
            go.GetComponent<Toggle>().isOn = GameManager.instance.onSound;
        }
		//GameManager.instance._audio.GetComponent<AudioControl> ().Behavior ();
    }
	
	// Update is called once per frame
	void Update () {
	
	}

    public void CLose()
    {
      
        GameManager.instance.ShowSetting(false);
    }


    public void Sound()
    {
       
        GameManager.instance.onSound = !GameManager.instance.onSound;
        var go=GameObject.Find("ToggleSound");
        if(go)
        {
            go.GetComponent<Toggle>().isOn = GameManager.instance.onSound;
        }
        GameManager.instance.setSound();
		GameManager.instance._audio.GetComponent<AudioControl> ().Behavior ();
    }

    public void Toggle()
    {
       
        var go = GameObject.Find("ToggleSound");
        if (go)
        {
            GameManager.instance.onSound = go.GetComponent<Toggle>().isOn;
        }
        GameManager.instance.setSound();
		GameManager.instance._audio.GetComponent<AudioControl> ().Behavior ();
    }

    public void Tut()
    {
        
        GameManager.instance.ShowInstructions (true);
    }

    public void Rate()
    {
        
        GameManager.instance.Rate();
    }

    public void Email()
    {
        
        GameManager.instance.Mailto();
    }
}
