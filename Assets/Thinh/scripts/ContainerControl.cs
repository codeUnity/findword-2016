﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ContainerControl : MonoBehaviour {

	public float width;
	public bool flag=true;
	// Use this for initialization
	void Start () {
		for (int i = 0; i < transform.childCount; i++) {
			var child = transform.GetChild (i);
			var com = child.GetComponent<LayoutElement> ();

			if (com != null) {
				print (Camera.main.aspect);
				if (Mathf.Abs (Camera.main.aspect - 9.0f / 16.0f) <= 0.001 && flag)
					return;

				float scale = 16.0f / 9.0f * Camera.main.aspect;
				com.preferredWidth = width * scale;
			}
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
