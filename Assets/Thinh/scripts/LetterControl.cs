﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using System.Collections.Generic;

public class LetterControl : MonoBehaviour {

    public bool _answer = false;
    public Vector2 location;


	public void Transition()
	{
		//GetComponent<Animator> ().SetTrigger ("Normal");
	}


    public List<Vector2> neighbors()
    {
        List<Vector2> temp = new List<Vector2>();
        temp.Add(new Vector2(location.x-1,location.y-1));
        temp.Add(new Vector2(location.x - 1, location.y));
        temp.Add(new Vector2(location.x - 1, location.y + 1));

        temp.Add(new Vector2(location.x, location.y - 1));
        temp.Add(new Vector2(location.x, location.y + 1));

        temp.Add(new Vector2(location.x + 1, location.y - 1));
        temp.Add(new Vector2(location.x + 1, location.y));
        temp.Add(new Vector2(location.x + 1, location.y + 1));
        return temp;
    }

    public static List<Vector2> neighbors(Vector2 location)
    {
        List<Vector2> temp = new List<Vector2>();
        temp.Add(new Vector2(location.x - 1, location.y - 1));
        temp.Add(new Vector2(location.x - 1, location.y));
        temp.Add(new Vector2(location.x - 1, location.y + 1));

        temp.Add(new Vector2(location.x, location.y - 1));
        temp.Add(new Vector2(location.x, location.y + 1));

        temp.Add(new Vector2(location.x + 1, location.y - 1));
        temp.Add(new Vector2(location.x + 1, location.y));
        temp.Add(new Vector2(location.x + 1, location.y + 1));
        return temp;
    }

    public static List<Vector2> neighbors(Vector2 location,int size)
    {
        List<Vector2> temp = new List<Vector2>();
        temp.Add(new Vector2(location.x - 1, location.y - 1));
        temp.Add(new Vector2(location.x - 1, location.y));
        temp.Add(new Vector2(location.x - 1, location.y + 1));

        temp.Add(new Vector2(location.x, location.y - 1));
        temp.Add(new Vector2(location.x, location.y + 1));

        temp.Add(new Vector2(location.x + 1, location.y - 1));
        temp.Add(new Vector2(location.x + 1, location.y));
        temp.Add(new Vector2(location.x + 1, location.y + 1));
		List<Vector2> temp1 = new List<Vector2>();
        foreach (var item in temp)
        {
            if ((item.x < size && item.y < size && item.x >= 0 && item.y >= 0))
            {
				temp1.Add(item);
            }
        }
		return temp1;
    }
}
