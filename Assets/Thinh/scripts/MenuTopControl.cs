﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MenuTopControl : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void Show()
    {
        GameManager.instance.ShowMoreHint(true);
    }

	public void Load()
	{
		print ("Load");
		var brain=transform.Find("top").Find ("brain").Find("Text");
		if (brain) {
			brain.GetComponent<Text> ().text = GameManager.instance.brain_size.ToString();
		}

		var percent = (int)(100.0f * GameManager.instance._current_word / GameManager.instance.total_word);

		var completed=transform.Find("top").Find ("btn_complete").Find("Text");
		if (completed) {
			
			completed.GetComponent<Text> ().text = percent.ToString () + " %";
		}

		var circle=transform.Find("top").Find ("btn_complete").Find("circle");
		if (circle) {
			var image = circle.GetComponent<Image> ();
			if (image.type == Image.Type.Filled) {
				image.fillAmount = percent / 100.0f;
			}
		}

		var hint=transform.Find("top").Find ("btn_hint").Find("Text");
		if (hint != null) {
			hint.GetComponent<Text> ().text = GameManager.instance.hints.ToString();
		}
	}
}
