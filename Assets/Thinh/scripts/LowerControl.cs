﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class LowerControl : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void Reset()
	{
		GameManager.instance.ResetGame ();
		GameManager.instance.ShowGame ();
	}

	public void Hint()
	{
        if (GameManager.instance.hints <= 0) {
			GameManager.instance.ShowMoreHint (true);
			var go = GameObject.Find ("btn_hint_unique").transform.Find ("Text");
			var str = string.Format ("HINT!({0})", GameManager.instance.hints);
			go.GetComponent<Text> ().text = str;
		}

		GameManager.instance.ShowHint ();
		Load ();
	}

	public void Load()
	{
		
			//print (GameManager.instance.hints);
			var go = GameObject.Find ("btn_hint_unique").transform.Find ("Text");
			var str = string.Format ("HINT!({0})", GameManager.instance.hints);
			go.GetComponent<Text> ().text = str;
		
	}

	public void ShowInstruction()
	{
        GameManager.instance.ShowInstructions (true);

	}
}
