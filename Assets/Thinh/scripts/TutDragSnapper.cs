﻿using System.Collections;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
/// <summary>
/// Drag snapper.
/// @author: v2tmobile
/// </summary>
public class TutDragSnapper : UIBehaviour, IEndDragHandler, IBeginDragHandler
{
	public ScrollRect scrollRect; // the scroll rect to scroll
	public int itemCount; // how many items we have in our scroll rect

	public AnimationCurve curve = AnimationCurve.Linear(0f, 0f, 1f, 1f); // a curve for transitioning in order to give it a little bit of extra polish
	public float speed; // the speed in which we snap ( normalized position per second? )

	protected void Start()
	{
		if (scrollRect == null) // if we are resetting or attaching our script, try and find a scroll rect for convenience 
			scrollRect = GetComponent<ScrollRect>();
	}

	public void OnBeginDrag(PointerEventData eventData)
	{
		pos = scrollRect.horizontalNormalizedPosition;
		StopCoroutine(SnapRect(0)); // if we are snapping, stop for the next input
	}

	public void OnEndDrag(PointerEventData eventData)
	{
		
		StartCoroutine(SnapRect(0)); // simply start our coroutine ( better than using update )
	}

	float pos;
	private IEnumerator SnapRect(int index)
	{
		if (scrollRect == null)
			throw new System.Exception("Scroll Rect can not be null");
		if (itemCount == 0)
			throw new System.Exception("Item count can not be zero");

		float startNormal = scrollRect.horizontalNormalizedPosition; // find our start position
		Debug.Log (startNormal);
		float delta = 1f / (float)(itemCount - 1); // percentage each item takes
		float dd=0;
		if (pos < startNormal)
			dd = 0.5f;
		else
			dd = -0.5f;
		print (dd);
		int target = Mathf.RoundToInt(startNormal / delta+dd); // this finds us the closest target based on our starting point
		float endNormal = delta * target + (float)(index) / (float)(itemCount-1); // this finds the normalized value of our target
		float duration = Mathf.Abs((endNormal - startNormal) / speed); // this calculates the time it takes based on our speed to get to our target
		print(endNormal);
		float timer = 0f; // timer value of course
		while (timer < 1f) // loop until we are done
		{
			timer = Mathf.Min(1f, timer + Time.deltaTime / duration); // calculate our timer based on our speed
			float value = Mathf.Lerp(startNormal, endNormal, curve.Evaluate(timer)); // our value based on our animation curve, cause linear is lame
			scrollRect.horizontalNormalizedPosition = value;
			//Debug.Log ("Horizontal:" + value);
			yield return new WaitForEndOfFrame(); // wait until next frame
		}
	}

	private IEnumerator SnapRect1(int index)
	{
		if (scrollRect == null)
			throw new System.Exception("Scroll Rect can not be null");
		if (itemCount == 0)
			throw new System.Exception("Item count can not be zero");

		float startNormal = scrollRect.horizontalNormalizedPosition; // find our start position
		//Debug.Log (startNormal);
		float delta = 1f / (float)(itemCount - 1); // percentage each item takes
		int target = Mathf.RoundToInt(startNormal / delta); // this finds us the closest target based on our starting point
		float endNormal = delta * target + (float)(index) / (float)(itemCount-1); // this finds the normalized value of our target
		float duration = Mathf.Abs((endNormal - startNormal) / 10.0f); // this calculates the time it takes based on our speed to get to our target

		float timer = 0f; // timer value of course
		while (timer < 1f) // loop until we are done
		{
			timer = Mathf.Min(1f, timer + Time.deltaTime / duration); // calculate our timer based on our speed
			float value = Mathf.Lerp(startNormal, endNormal, curve.Evaluate(timer)); // our value based on our animation curve, cause linear is lame
			scrollRect.horizontalNormalizedPosition = value;
			//Debug.Log ("Horizontal:" + value);
			yield return new WaitForEndOfFrame(); // wait until next frame
		}
	}

	public void Next()
	{
		StartCoroutine(SnapRect(1));
	}

	public void Previous()
	{
		StartCoroutine(SnapRect(-1));
	}

	public void Previous(int index)
	{
		StartCoroutine(SnapRect(-index));
	}

	public void NextInmediate(int index)
	{
		StartCoroutine(SnapRect1(index));
	}
}