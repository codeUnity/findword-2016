﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.IO;
using UnityEngine.UI;

public enum Layer
{
	Menu,
	Category,
	Level,
	Game,
	Complete

}
public class GameManager : MonoBehaviour
{
	//public static string url = "http://app.pixa.vn/findword/english.json";
	public GameObject btn_back;
	public GameObject _scrollrect;
	public GameObject _category_go;
	public GameObject _level_go;
	public GameObject _game_go;
	public GameObject _menu_go;
	public GameObject _completed_go;
    public GameObject _setting_go;
    public GameObject _more_hints_go;
	public GameObject _pass;
	public GameObject _create_go;
	public GameObject _category_list_go;
	public GameObject _instructions;

	public GameObject _level_list_go;
	public GameObject _puzzle_go;

	public GameObject fetch_go;

	public GameObject start_go;

	public GameObject _audio;

    public GameObject _unlock_go;

    

	public Layer _current;

    public string _word;
	public string _word_temp;
    public List<string> _list;
	public List<string> _list_temp;

	public int _category;
	public int _max_category;
	public int _max_level;
	public int _level;
	public int _size;

	public bool _stateplay = true;

	public int brain_size;
	public int _current_word;

	public Data data;

	public int total_word = 0;

	public int hints=3;
	public int hint_used = 0;
    public bool show_ads=true;

	public bool show_instructions=false;

	public bool _backing=false;

    public bool can_back;

    private static GameManager _instance;

	public static GameManager instance {
		get {
			if (_instance == null) {
				_instance = GameObject.FindObjectOfType<GameManager> ();
				DontDestroyOnLoad (_instance.gameObject);
			}
			return _instance;
		}
	}
	void Awake ()
	{
		if (_instance == null) {
			_instance = this;
			DontDestroyOnLoad (this);
		} else {
			if (this != _instance)
				Destroy (this.gameObject);
		}
        //Prepare();


        //LoadURL();
        LoadData();

		Init ();
		//Guid g = Guid.NewGuid();
        //print(g.ToString());
        //StartCoroutine(Post());
        //Read ();

        //Write ();
    }

	public GameObject board_create;

	public void BoardCreate()
	{
		board_create.GetComponent<BoardCreateControl> ().Reset ();
		board_create.GetComponent<BoardCreateControl> ().Init ();
	}
	public void Init()
	{
        Vungle.init("56974a7c1440823e0100007d", "56b16f869c2456602200000f", "WP");
		Vungle.onAdFinishedEvent+= (AdFinishedEventArgs arg) => {
			hints+=3;
			Save();
			ShowMenu();
			ShowMoreHint(false);
			print("Finish");
		};
        

    }


	void OnApplicationPause(bool pauseStatus) {
		if (pauseStatus) {
			Vungle.onPause();
		}
		else {
			Vungle.onResume();
            if (PlayerPrefs.GetInt("fisrtbonus", 0) == 0)
            {
                PlayerPrefs.GetInt("fisrtbonus", 1);
                PlayerPrefs.SetInt("firstday", DateTime.Now.Day);

            }
            var firstday = PlayerPrefs.GetInt("firstday", 0);
            var current = DateTime.Now.Day;
            var before = PlayerPrefs.GetInt("day", 0);
            if (current != before && current!= firstday)
            {
                PlayerPrefs.SetInt("firstday", -1);
                ShowBonus(true);
                //Load();

                PlayerPrefs.SetInt("day", current);
            }

        }
	}

    public void Prepare()
    {
        var temps = _word.Split(' ');
		_list.Clear ();
		_list_temp.Clear ();
        foreach (var item in temps)
        {
            _list.Add(item);
			_list_temp.Add (item);
        }

		_list.Sort (delegate(string x, string y) {
			if(x.Length>y.Length)
				return -1;
			else if(x.Length<y.Length)
				return 1;
			else return 0;
		});
		_word_temp = "";
		_word_temp=_word_temp.Trim ();	
    }


	public void LoadURL()
	{
        GameManager.instance.can_back = true;
        StartCoroutine(CheckConnection());
        //StartCoroutine (LoadData ());

	}


	public static string api= "http://app.pixa.vn/findword/game.php?action=load&value=";
    IEnumerator Fetch()
	{
		WWW www = new WWW(linkpost);
        yield return www;
        //url=www.text;
		var str=www.text.Replace("\t",string.Empty);
        print(www.text);
		EndFetch (str);
    }


	public void EndFetch(string value)
	{
		if (!value.Equals ("")) {
			float sqrt = Mathf.Sqrt (value.Length);
			int size = Mathf.RoundToInt (sqrt);
			print ("Size: " + size);
			print (value);
			print ("Length: " + value.Length);
			if (size*size<value.Length)
				size++;
			_size = size;
			statefetch = true;
			BackSlide ();
			_word = value.ToUpper ();
			Prepare ();
			ShowGame ();
			ShowFetch (false);
			_current+=3;
			_scrollrect.GetComponent<DragSnapper> ().NextInmediate (3);
		}
	}

	public void FetchPuzzle(string value)
	{
		
		GameManager.instance.ResetGame ();
		linkpost = api + value;
		StopAllCoroutines();
		StartCoroutine(Fetch());
	}

	public static string apipost = "http://app.pixa.vn/findword/game.php?action=generate";//?action=generate&id={";

	public string linkpost="";
	string value;
    public void PostPuzzle(string value)
    {
		this.value = value;
        StopAllCoroutines();
        StartCoroutine(Post());
    }
    IEnumerator Post()
    {
		WWWForm form = new WWWForm();
		form.AddField("id", value);
		WWW www = new WWW (apipost, form);
        yield return www;
        //url=www.text;
		var str=www.text.Replace("\t",string.Empty);
		print(www.text.Replace("\t",string.Empty));
		GameManager.instance.Next ();
		ShowLevel (false);
		var code=GameObject.Find ("code");
		if (code) {
			code.GetComponent<Text> ().text = str.ToUpper ();
		}
    }
    bool hasInternet = true;
    IEnumerator CheckConnection()
    {
        const float timeout = 2f;
        float startTime = Time.timeSinceLevelLoad;
        var ping = new Ping("8.8.8.8");
        
        while (true)
        {
            //internetAvailableText = "Checking network...";
            if (ping.isDone)
            {
                hasInternet = true;
                //internetAvailableText = "Network available.";
                print("Network available.");
                break;
            }
            if (Time.timeSinceLevelLoad - startTime > timeout)
            {
                hasInternet = false;
                //internetAvailableText = "No network.";
                print("No network.");
                break;
            }

            yield return new WaitForEndOfFrame();
        }
        //StartCoroutine(LoadData());

    }

    public void SendNotification()
    {

#if UNITY_ANDROID
        var current=DateTime.Now.TimeOfDay.Ticks/1000000;
        print(current);
        long timeout = 0;
        if(current>= 68400)
        {
            timeout = current + 86400 - 68400;

        }
        else
        {
            timeout = current;
        }

        LocalNotification.SendRepeatingNotification(1, 86400, timeout, "Hello", "Obtain 5 hints every day!", new Color32(0xff, 0x44, 0x44, 255));
#elif UNITY_IOS
        var ta = File.ReadAllText(Application.dataPath + "/data.json");;
        if (Application.platform == RuntimePlatform.IPhonePlayer)
        {
            DateTime now = DateTime.Now;
            DateTime today7pm = now.Date.AddHours(3);
            DateTime next7pm = now <= today7pm ? today7pm : today7pm.AddDays(1);

            UnityEngine.iOS.LocalNotification notification = new UnityEngine.iOS.LocalNotification();
            notification.fireDate = next7pm;
            notification.alertAction = "Hello";
            notification.alertBody = "Obtain 5 hints every day!";
            notification.repeatInterval = UnityEngine.iOS.CalendarUnit.Day;
            notification.hasAction = false;
            UnityEngine.iOS.NotificationServices.ScheduleLocalNotification(notification);
        }
#endif
    }

   // IEnumerator LoadData() {
    void LoadData() {
            //yield return new WaitForSeconds(5.1f);
            //string wwwtext = "";
     
        /*if(hasInternet)
        {
            WWW www = new WWW(url);
            yield return www;
            wwwtext = www.text;
        }*/
        
       
		//url=www.text;
        var wwwtext = (TextAsset)Resources.Load("data", typeof(TextAsset));
        print (wwwtext);
		var total_word_local = 0;
		Data _data_online=null;
		if (!wwwtext.Equals ("")) {
			_data_online = JsonUtility.FromJson<Data> (wwwtext.text);


		}

        //var text_offline = PlayerPrefs.GetString ("data", "");
        var text_offline = (TextAsset)Resources.Load("data", typeof(TextAsset));

        Data _data_offline=null;
		if (!text_offline.Equals ("")) {
			_data_offline = JsonUtility.FromJson<Data> (text_offline.text);

		}

		if (_data_offline == null) {
            var ta = (TextAsset)Resources.Load("data", typeof(TextAsset));
            _data_offline = JsonUtility.FromJson<Data> (ta.text);
		}

		if (_data_online != null) {
			if (_data_online.version > _data_offline.version) {
				data = _data_online;
				PlayerPrefs.SetString ("data", wwwtext.text);
			} else
				data = _data_offline;
		} else
			data = _data_offline;

        int count_subject = 0;
		foreach (var item in data.subjects) {
            //print(item.brain);
            count_subject++;
			foreach (var u in item.words) {
				total_word_local++;
				string key = item.name + total_word.ToString ();
				item.hint = PlayerPrefs.GetInt (key, 0);
				item.solve=(PlayerPrefs.GetInt(key+"solve",0)==1)?true:false;
                if (count_subject <= 20 || data.version >=20)
                    item.show = true;
                else item.show = false;
			}
		}

		total_word = total_word_local;
		Load ();
		Save ();
		ShowMenu ();
		Begin();
		
		BoardCreate ();

        if(PlayerPrefs.GetInt("notify",0)==0)
        {
            SendNotification();
            PlayerPrefs.SetInt("notify", 1);
        }
           
    }

	public void Read()
	{
        //var ta = File.ReadAllText(Application.dataPath + "/data.json");
        var ta = (TextAsset)Resources.Load("data", typeof(TextAsset));
        var obj = JsonUtility.FromJson<Data> (ta.text);
		total_word = 0;
		foreach (var item in obj.subjects) {
			print (item.name);
			print (item.size);
			print (item.brain);
			foreach (var u in item.words) {
				//print (u);
				total_word++;
				string key = item.name + total_word.ToString ();
				item.hint = PlayerPrefs.GetInt (key, 0);
				item.solve=(PlayerPrefs.GetInt(key+"solve",0)==1)?true:false;
			}
		}
		data = obj;
		Load ();
		Save ();
		ShowMenu ();
		Begin();
	}


	public void Begin()
	{
		Invoke ("InvokeBegin", 0.5f);
	}

	void InvokeBegin()
	{
		_audio.GetComponent<AudioControl> ().Behavior ();
		start_go.GetComponent<StartControl> ().Slide ();
	}
	public void Write()
	{
		Subject s = new Subject{ brain = 1, name = "hello", words = new List<string>{ "a", "b" } };
		Subject s2 = new Subject{ brain = 1, name = "hello", words = new List<string>{ "a", "b","c" } };
		Data data = new Data{ version = 1};
		data.subjects = new List<Subject> ();
		data.subjects.Add (s);
		data.subjects.Add (s2);
		string json = JsonUtility.ToJson (data,true);
		print (json);
		var obj = JsonUtility.FromJson<Data> (json);
		print (obj.version);
		print (obj.subjects.Count);

	}

	public bool statefetch=false;
	public void ShowFetch(bool state)
	{
		fetch_go.SetActive (state);
		var input=fetch_go.transform.Find ("InputField");
		if (input) {
			input.GetComponent<InputField> ().text = "";
		}
	}

	public void ResetGame()
	{
		_game_go.transform.Find ("board").GetComponent<BoardControl>().Reset();
		_game_go.transform.Find ("answer").GetComponent<AnswerControl>().Reset();
	}

	public void ShowGame()
	{
		_game_go.transform.Find ("board").GetComponent<BoardControl>().Init();
		_game_go.transform.Find ("answer").GetComponent<AnswerControl>().Init();
	}

	public void ShowCategory(bool state)
	{
		_stateplay = state;
		_create_go.SetActive (!state);
		_category_list_go.SetActive (state);
		if (state) {
			_category_go.GetComponentInChildren<ListControl> ().Reset ();
			_category_go.GetComponentInChildren<ListControl> ().Init ();
		} else {
			_create_go.GetComponent<CreateControl> ().Init ();
		}
		//_category_go.transform.Find ("List").gameObject.SetActive(state);
		//_category_go.transform.Find ("Create").gameObject.SetActive(state);
	}

	public void ShowLevel(bool state)
	{
		_stateplay = state;
		_level_list_go.SetActive (state);
		_puzzle_go.SetActive (!state);
		if (state) {
			_level_go.GetComponentInChildren<ListLevelControl> ().Reset ();
			_level_go.GetComponentInChildren<ListLevelControl> ().Init ();
		}
	}


	public void ShowCompleted()
	{
		Next ();
		_completed_go.GetComponent<CompleteControl> ().Init ();
	}

	public void ShowPass()
	{
        //var ad = GameObject.Find("AdmobService");
        //if(ad!=null)
        //{
        //    ad.GetComponent<AdmobServiceController>().ShowInterstitial();
        //}
		//btn_back.SetActive (false);
		if (statefetch) {
			ShowCompleted ();
            GameManager.instance.can_back = true;
            statefetch = false;
		}else
		if (_level+1 > data.subjects [_category - 1].words.Count && _category == _max_category && _level == _max_level) {
			ShowCompleted ();
            GameManager.instance.can_back = true;
        }
		else
		{
			BackIdle();
			_pass.SetActive (true);
			_pass.transform.Find ("star").GetComponent<StarControl>().Move();
			_scrollrect.SetActive (false);

			//Next ();
			SetWord ();
			Prepare ();
			//Invoke ("ReShowGame", 2.0f);
		}
		//GameManager.instance.hint_used = 0;
	}

	public void ReShowGame()
	{
		ResetGame ();
		//btn_back.SetActive (true);
		BackSlide();
		_pass.SetActive (false);
		_scrollrect.SetActive (true);
		//Previous ();
		ShowGame ();
        GameManager.instance.can_back = true;
    }

	public void ReComplete()
	{
		ResetGame ();
		ShowGame ();
	}


	public void Next()
	{
		_current++;
		_scrollrect.GetComponent<DragSnapper> ().Next ();
	}

	public void Previous()
	{
		//_backing = false;
		_current--;
		_scrollrect.GetComponent<DragSnapper> ().Previous ();
	}

	public void Previous(int index)
	{
		_current-=index;
		_scrollrect.GetComponent<DragSnapper> ().Previous (index);
	}

	public void BackSlide()
	{
		btn_back.GetComponent<Animator> ().SetTrigger ("slide");
	}

	public void BackIdle()
	{
		btn_back.GetComponent<Animator> ().SetTrigger ("idle");
	}



	public void SetWord()
	{
		//PlayerPrefs.SetInt ();
		string key=data.subjects[_category-1].name+_level;
	
		if (_category == _max_category && _level == _max_level) {
			PlayerPrefs.SetInt (key, hint_used);
			GameManager.instance.hint_used = 0;
			PlayerPrefs.SetInt (key + "solve", 1);
			_current_word++;
			GameManager.instance.brain_size += data.subjects [_category - 1].brain;
		}


		_level++;
		if (_level > data.subjects [_category - 1].words.Count) {
			_category++;
			_level = 1;
			_max_level = 1;
			_size = data.subjects [_category - 1].size;
			if(_category>_max_category)
				_max_category = _category;
		}


		if(_level>=_max_level && _category==_max_category)
			_max_level = _level;

		Save ();
		_word = data.subjects [_category - 1].words [_level - 1].ToUpper ();

	}

	public void ShowHint()
	{
		if (hints > 0) {
			//hints--;
			//hint_used++;
			var go = GameObject.Find ("answer");
			if (go) {
				go.GetComponent<AnswerControl> ().Hint ();
			}
		}
	}

	public void Save()
	{
		PlayerPrefs.SetInt ("category", _category);
		PlayerPrefs.SetInt ("level", _level);
		PlayerPrefs.SetInt ("size", _size);
		PlayerPrefs.SetInt ("max_category",_max_category);
		PlayerPrefs.SetInt ("max_level",_max_level);
		PlayerPrefs.SetInt ("brain_size", brain_size);
		PlayerPrefs.SetInt ("_current_word", _current_word);
		PlayerPrefs.SetInt ("hints", hints);

        setSound();

	}

	public void Load()
	{
		//PlayerPrefs.DeleteAll ();
		_category=PlayerPrefs.GetInt ("category",1 );
		_max_category=PlayerPrefs.GetInt ("max_category",1);
		_level=PlayerPrefs.GetInt ("level",1 );
		_max_level=PlayerPrefs.GetInt ("max_level",1 );
		_size=PlayerPrefs.GetInt ("size", 2);
		brain_size=PlayerPrefs.GetInt ("brain_size", 0);

		_current_word=PlayerPrefs.GetInt ("_current_word", 0);
		hints=PlayerPrefs.GetInt ("hints", 20);

        getSound();

    }

	public void ShowMenu()
	{
		_menu_go.GetComponent<MenuTopControl> ().Load ();
		var go=GameObject.Find ("lower_unique");
		if (go) {
			go.GetComponent<LowerControl> ().Load ();
		}
	}


    public bool onSound=true;

    public void setSound()
    {
        int value = onSound ? 1 : 0;
        PlayerPrefs.SetInt("Sound", value);
        PlayerPrefs.Save();
    }

    public void getSound()
    {
        int value = PlayerPrefs.GetInt("Sound", 1);
        onSound = value == 1 ? true : false;
    }


    public void ShowSetting(bool state)
    {
        _setting_go.SetActive(state);
        var go = GameObject.FindObjectOfType<SettingControl>();
        if(go!=null)
            go.GetComponent<SettingControl>().Init();
        //_scrollrect.SetActive(!state);
    }

    public void ShowMoreHint(bool state)
    {
        _more_hints_go.SetActive(state);
		show_instructions = state;
    }

	public void ShowInstructions(bool state)
	{
		_instructions.SetActive(state);
		show_instructions = state;
	}

    public GameObject bonus;

    public void ShowBonus(bool state)
    {
        bonus.SetActive(state);
        show_instructions = state;
    }

    public void ShowUnlock(bool state)
    {
        _unlock_go.SetActive(state);
        show_instructions = state;
    }

    public void Rate()
    {
#if UNITY_ANDROID
        Application.OpenURL("market://details?id=com.pixa.findword");
		GameManager.instance.hints+=2;
		GameManager.instance.Save();
#elif UNITY_IPHONE
 Application.OpenURL("itms-apps://itunes.apple.com/app/1074674966");
#endif
    }

    public string email;
    public string subject;
    public string body;
    public void Mailto()
    {
        Application.OpenURL("mailto:" + email + "?subject:" + subject + "&body:" + body);
    }

}