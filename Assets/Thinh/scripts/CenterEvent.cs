﻿using UnityEngine;
using System.Collections;

public class CenterEvent : MonoBehaviour {

    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void PlayClick()
	{
		GameManager.instance.Next ();
		GameManager.instance.BackSlide ();
		GameManager.instance.ShowCategory (true);
    }

    public void SettingClick()
    {
        GameManager.instance.ShowSetting(true);
    }

	public void CreatePuzzle()
	{
		GameManager.instance.Next ();
		GameManager.instance.BackSlide ();
		GameManager.instance.ShowCategory (false);
    }

	public void FetchPuzzle()
	{
		//GameManager.instance.
		//GameManager.instance.FetchPuzzle("w49DUL");
		GameManager.instance.ShowFetch(true);
        GAv3Control.instance.GetComponent<GoogleAnalyticsV3>()
      .LogEvent(new EventHitBuilder()
          .SetEventCategory("FetchPuzzle")
          .SetEventAction("Click"));
    }
}
